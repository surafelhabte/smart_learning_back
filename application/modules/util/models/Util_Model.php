<?php

   class Util_Model extends CI_Model
   {  
      public function __construct()
      {
        parent::__construct();
        $this->load->library('session');

      }
              
      public function Is_logged_in(){
        return $this->session->userdata('active_user');
      }

   }
