<?php
use Restserver\Libraries\REST_Controller;
use Restserver\Libraries\REST;

defined('BASEPATH') or exit('No direct script access allowed');
require_once APPPATH . 'libraries/REST_Controller.php';
require_once APPPATH . 'libraries/Format.php';
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, OPTIONS");

class Util extends CI_Controller
{
    use REST_Controller {
        REST_Controller::__construct as private __resTraitConstruct;
  }

    public function __construct()
    {
        parent::__construct();
        $this->__resTraitConstruct();
        $this->load->library('session');

        $this->load->model('Util_Model');
    }
    
    public $roles =[ 
        ['claimType' => 'canViewCertification', 'claimValue' => false],
        ['claimType' => 'canAddCertification', 'claimValue' => true],
        ['claimType' => 'canUpdateCertification', 'claimValue' => true],
        ['claimType' => 'canDeleteCertification', 'claimValue' => true],
        ['claimType' => 'canViewEnrollment', 'claimValue' => true],
        ['claimType' => 'canAddEnrollment', 'claimValue' => true],
        ['claimType' => 'canUpdateEnrollment', 'claimValue' => true],
        ['claimType' => 'canDeleteEnrollment', 'claimValue' => true],
        ['claimType' => 'canViewRequest', 'claimValue' => true],
        ['claimType' => 'canAddRequest', 'claimValue' => true],
        ['claimType' => 'canUpdateRequest', 'claimValue' => true],
        ['claimType' => 'canDeleteRequest', 'claimValue' => true],
        ['claimType' => 'canViewDetailRequest', 'claimValue' => true],
        ['claimType' => 'canRequestForOther', 'claimValue' => true],
        ['claimType' => 'canViewProgram', 'claimValue' => true],
        ['claimType' => 'canAddProgram', 'claimValue' => true],
        ['claimType' => 'canUpdateProgram', 'claimValue' => true],
        ['claimType' => 'canDeleteProgram', 'claimValue' => true],
        ['claimType' => 'canViewProvider', 'claimValue' => true],
        ['claimType' => 'canAddProvider', 'claimValue' => true],
        ['claimType' => 'canUpdateProvider', 'claimValue' => true],
        ['claimType' => 'canDeleteProvider', 'claimValue' => true],
        ['claimType' => 'canViewTraining', 'claimValue' => true],
        ['claimType' => 'canAddTraining', 'claimValue' => true],
        ['claimType' => 'canUpdateTraining', 'claimValue' => true],
        ['claimType' => 'canDeleteTraining', 'claimValue' => true],
        ['claimType' => 'canViewDashboard', 'claimValue' => true],
        ['claimType' => 'canViewReport', 'claimValue' => true],
    ];
 
    public function Is_logged_in_get()
    {
        $result = $this->Util_Model->Is_logged_in();
        $this->response($result, REST::HTTP_OK);
        
    }

    public function Roles_get()
    {
        $this->response($this->roles, REST::HTTP_OK);
    }

    public function Logout_get()
    {
        $result = $this->session->unset_userdata('active_user');
        $this->response($result, REST::HTTP_OK);
    }

   
}
