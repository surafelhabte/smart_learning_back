<?php
use Restserver\Libraries\REST_Controller;
use Restserver\Libraries\REST;

defined('BASEPATH') or exit('No direct script access allowed');
require_once APPPATH . 'libraries/REST_Controller.php';
require_once APPPATH . 'libraries/Format.php';
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, OPTIONS");

class Report extends CI_Controller
{
    use REST_Controller {
        REST_Controller::__construct as private __resTraitConstruct;
  }

    public function __construct()
    {
        parent::__construct();
        $this->__resTraitConstruct();
        
        $this->load->library('Validate_Token');
        $this->load->model('Report_Model');
    }
 
    public function Submission_post()
    {
        $response = $this->validate_token->authenticateToken($this->input->request_headers());
        if ($response) {
            $result = $this->Report_Model->Submission($this->post());
            $this->response($result, REST::HTTP_OK);

        } else {
            $this->response(['Not authorized'], REST::HTTP_OK);

        }
    }

    public function Certification_post()
    {
        $response = $this->validate_token->authenticateToken($this->input->request_headers());
        if ($response) {
            $result = $this->Report_Model->Certification($this->post());
            $this->response($result, REST::HTTP_OK);

        } else {
            $this->response(['Not authorized'], REST::HTTP_OK);

        }
    }

    public function Departments_get()
    {
        $response = $this->validate_token->authenticateToken($this->input->request_headers());
        if ($response) {
            $result = $this->Report_Model->Departments();
            $this->response($result, REST::HTTP_OK);

        } else {
            $this->response(['Not authorized'], REST::HTTP_OK);

        }
    }

    public function Dashboard_get()
    {
        $response = $this->validate_token->authenticateToken($this->input->request_headers());
        if ($response) {
            $result = $this->Report_Model->Dashboard();
            $this->response($result, REST::HTTP_OK);

        } else {
            $this->response(['Not authorized'], REST::HTTP_OK);

        }
    }

    public function Employees_post()
    {
        $response = $this->validate_token->authenticateToken($this->input->request_headers());
        if ($response) {
            $result = $this->Report_Model->Employees($this->post());
            $this->response($result, REST::HTTP_OK);

        } else {
            $this->response(['Not authorized'], REST::HTTP_OK);

        }
    }


 
  
}
