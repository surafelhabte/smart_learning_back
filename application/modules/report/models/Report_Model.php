<?php

   class Report_Model extends CI_Model
   {
       public function __construct()
       {
           parent::__construct();
           $this->core_Db=config_item('core_db');
       }

        public function Submission($post)
        {
            $post['fiscal_year'] ?? $post['fiscal_year'] = date('Y');

            $table = $post['table'];                  

            $result =  $this->db->select("department_name,submission_status,count(dep.id) as total_employee,dep.id")
                            ->from("$this->core_Db.department as dep")
                            ->join("$this->core_Db.position as pos","pos.department_id = dep.id")
                            ->join("$this->core_Db.employee_data as emp","emp.position_id = pos.id")
                            ->join("tbl_apa as apa","apa.employee_id = emp.employee_id")
                            ->group_by(["dep.id","submission_status"])
                            ->where(["Year(apa.created_at)"=>$post['fiscal_year']])
                            ->get()->result(); 

                            return $result;

            $this->form_validation->set_rules($id)->set_data($post);

            if($this->form_validation->run()) {


            } else {
                return ['status' => false, 'message' => implode('\n', $this->form_validation->error_array())];

            }         
        }

        public function Certification($post){
            $select = "IFNULL(training,title) as Training, IFNULL(date,cert.created_at) as Date,Result";
        
            return $this->db->select($select)
                            ->from("tbl_certification as cert")
                            
                            ->join("tbl_enrollment as enr","enr.id = cert.enrollment_id","left")
                            ->join("tbl_program as pro","pro.id = enr.program_id","left")
                            ->join("tbl_training as tra","tra.id = pro.training_id","left")

                            ->where("cert.employee_id = '$post[employee_id]'")

                            ->order_by("date")
                            ->get()->result();
        }

        public function Departments(){
            return $this->db->select("id as value,department_name as text")
                            ->get("$this->core_Db.department")
                            ->result();

        }

        public function Employees($post){
            return $this->db->select('emp.employee_id as value,concat(concat_ws(" ",first_name,middle_name,last_name)," => ",department_name) as text')
                            ->from("$this->core_Db.department as dep")
                            ->join("$this->core_Db.position as pos","pos.department_id = dep.id")
                            ->join("$this->core_Db.employee_data as emp","emp.position_id = pos.id")  

                            ->or_like('first_name',$post['keyword'])
                            ->or_like('middle_name',$post['keyword'])
                            ->order_by("first_name")
                            ->get()->result();

        }

        public function Dashboard(){
            $result = null;
            $result = (Object)$result;

            $result->request = $this->db->select("count(id) as id")                                        ->get_where("tbl_request","YEAR(created_at) = YEAR(NOW())")
                                        ->result()[0]->id;

            $result->program = $this->db->select("count(id) as id")                                        ->get_where("tbl_program","YEAR(fiscal_year) = YEAR(NOW())")
                                        ->result()[0]->id;

            $result->certificate = $this->db->select("count(id) as id")
                                            ->get_where("tbl_certification","YEAR(created_at) = YEAR(NOW())")
                                            ->result()[0]->id;

            $result->enrollment = $this->db->select("count(id) as id")
                                            ->get_where("tbl_enrollment","YEAR(created_at) = YEAR(NOW())")
                                            ->result()[0]->id;

            $result->cost = $this->db->select_sum("cost","cost")
                                    ->get_where("tbl_program","YEAR(fiscal_year) = YEAR(NOW())")
                                    ->result()[0]->cost;

            $result->closed = $this->db->select("count(id) as id")
                                       ->get_where("tbl_program","YEAR(fiscal_year) = YEAR(NOW()) AND end_date < NOW()")
                                       ->result()[0]->id;
            
            return $result;                           
           
        }
    
   }