<?php

   class Notification_Model extends CI_Model
   {
        public function __construct()
        {
           parent::__construct();
           $this->core_Db=config_item('core_db');                
        }

        public function Schedule(){

           $result = $this->db->select('program_beginning_notification as pb')->get('tbl_configuration')->row();

           $select = "pro.id,emp.employee_id,email,concat_ws(' ','Dear',concat(first_name,','),tra.title,
                      'program will be begin in',start_date,message) as message";

           $result = $this->db->select($select)
                              ->from("$this->core_Db.employee_data as emp")
                              ->join("$this->core_Db.contact_info as con","con.employee_id = emp.employee_id")

                              ->join("tbl_enrollment as enr","enr.employee_id = emp.employee_id")
                              ->join("tbl_program as pro","pro.id = enr.program_id")
                              ->join("tbl_training as tra","tra.id = pro.training_id")

                              ->join("tbl_sent_notifications as se_noti","se_noti.employee_id != emp.employee_id 
                                    AND se_noti.notification_id = '1' AND se_noti.program_id = pro.id","Left")
                              ->join("tbl_notification as noti","noti.id = '1'")
                               ->where("((start_date - current_date()) <= $result->pb)
                                 AND emp.employee_id not in (SELECT employee_id FROM tbl_sent_notifications WHERE notification_id = '1')")
                              
                              ->get()->result();

            return $result;                  

            if(count($result) > 0){
               $this->load->helper("email");

               $this->db->trans_begin();

               foreach($result as $v){
                  if(send($v->email,$v->message,'Notification')){
                     $data = ['employee_id'=>$v->employee_id,'notification_id'=>1,'program_id'=>$v->id];
                     $this->db->insert('tbl_sent_notifications',$data);
                  }
               }

               $this->db->trans_status() === true ?  $this->db->trans_commit() : $this->db->trans_rollback();
                 
            }

        }
        
   }
?>
