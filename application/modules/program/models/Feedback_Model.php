<?php

   class Feedback_Model extends CI_Model
   {
        public function __construct()
        {
           parent::__construct();
           $this->load->helper('uuid_gen');
           $this->core_Db=config_item('core_db');
        }
        
        public function Create($post){
            $this->form_validation->set_rules($this->validation())->set_data($post);

            if($this->form_validation->run()) {
                $post['id'] = uuid_gen();

                $this->db->trans_begin();
                $this->db->insert('tbl_feedback', $post);

                if ($this->db->trans_status() === true) {
                    $this->db->trans_commit();
                    return ['status'=>true, 'message' =>'feedback submitted successfully.'];
    
                } else {
                    $this->db->trans_rollback();
                    return ['status'=>false, 'message' =>'unable to submit the feedback.'];
    
                }

            } else {
                return ['status' => false, 'message' => implode('\n', $this->form_validation->error_array())];

            }
        }
    
        public function Get($id)
        {
            $select = 'fb.id,fb.employee_id,concat_ws(" ",first_name,middle_name) as employee,comment,fb.created_at';

            return $this->db->select($select)
                            ->from("tbl_feedback as fb")
                            ->join("$this->core_Db.employee_data as emp","emp.employee_id = fb.employee_id")
                            ->where("enrollment_id = $id")
                            ->get()->result(); 

        }

        public function Delete($id){
            $this->db->trans_begin();
            $this->db->delete('tbl_feedback', ['id' => $id]);

            if ($this->db->trans_status() === true) {
                $this->db->trans_commit();
                return ['status'=>true, 'message' =>'feedback deleted successfully.'];

            } else {
                $this->db->trans_rollback();
                return ['status'=>false, 'message' =>'unable to delete the feedback.'];

            }
        }

        // Validation
        private function validation(){
            return [
                ['field' => 'employee_id','label' => 'employee_id','rules' => 'required'],
                ['field' => 'comment','label' => 'comment','rules' => 'required'],
                ['field' => 'enrollment_id','label' => 'enrollment','rules' => 'required'],
            ];
        }
 
   }