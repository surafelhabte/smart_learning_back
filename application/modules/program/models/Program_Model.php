<?php

   class Program_Model extends CI_Model
   {

        public function __construct()
        {
           parent::__construct();
           $this->load->helper('uuid_gen');

        }

        public function Create($post){
            $this->form_validation->set_rules($this->validation())->set_data($post);
                        
            if($this->form_validation->run()) {
                $post['id'] = uuid_gen();

                $this->db->trans_begin();
                $this->db->insert('tbl_program', $post);                

                if ($this->db->trans_status() === true) {
                    $this->db->trans_commit();
                    return ['status'=>true, 'message' =>'program saved successfully.'];
    
                } else {
                    $this->db->trans_rollback();
                    return ['status'=>false, 'message' =>'unable to save the program.'];

                }

            } else {
                return ['status' => false, 'message' => implode('\n', $this->form_validation->error_array())];

            }
        }

        public function Update($post){   

            $result = $this->db->from('tbl_certification as cert')
                               ->join('tbl_enrollment as enr',"enr.id = cert.enrollment_id")  
                               ->join('tbl_program as pro',"pro.id = enr.program_id")  
                               ->where(['pro.id'=>$post['id']])->count_all_results();
            
            if($result > 0){
                return ['status'=>false, 'message' =>"you can't update program now. b\c certification already given."];
                
            } else {
                $id =  ['field' => 'id','label' => 'id','rules' => 'required'];
                $this->form_validation->set_rules(array_merge($this->validation(),$id))->set_data($post);
        
                if($this->form_validation->run()) {                
                        
                    $this->db->trans_begin();
                    $this->db->update('tbl_program',$post, ['id' => $post['id']]);
    
                    if($this->db->trans_status() === TRUE){
                        $this->db->trans_commit();
                        return ['status'=>true,'message'=>'program updated successfully.'];
            
                    } else {
                        $this->db->trans_rollback();
                        return ['status'=>false,'message'=>'unable to update program.'];
                    }
    
                } else {
                    return ['status' => false, 'message' => implode('\n', $this->form_validation->error_array())];
    
                }
            }
    
        }
    
        public function Filter($post){
            $select = 'pro.id,YEAR(fiscal_year) as fiscal_year,cost,concat_ws(" - ",start_date,end_date) as schedule,title,name';
            
            !is_null($post['fiscal_year']) ? $where = "YEAR(fiscal_year) = '$post[fiscal_year])'" : $where = "YEAR(fiscal_year) <= YEAR(NOW())";

            return $this->db->select($select)
                            ->from("tbl_program as pro")
                            ->join("tbl_training as tra","tra.id = pro.training_id")
                            ->join("tbl_training_provider as tp","tp.id = pro.training_provider_id")
                            ->order_by("fiscal_year")
                            ->where("$where")
                            ->get()->result(); 
        }
     
        public function Get($id){ 
            $select = "id, fiscal_year, cost, start_date, end_date, training_id, training_provider_id";

            return $this->db->select($select)->get_where("tbl_program", ["id" => $id])->row();

        }
      
        public function Delete($id){
            $result = $this->db->from('tbl_enrollment')->where(['program_id'=>$id])->count_all_results();
                   
            if($result > 0){
                return ['status'=>false, 'message' =>"you can't delete program now. b\c someone already enrolled."];
                
            } else {
                $this->db->trans_begin();
                $this->db->delete('tbl_program', ['id' => $id]);
    
                if ($this->db->trans_status() === true) {
                    $this->db->trans_commit();
                    return ['status'=>true, 'message' =>'program deleted successfully.'];
    
                } else {
                    $this->db->trans_rollback();
                    return ['status'=>false, 'message' =>'unable to delete the program.'];
    
                }
            }

        }

        public function Trainings(){            
            return $this->db->select("id,title")
                            ->from("tbl_training")
                            ->where("id not in (select training_id from tbl_program where YEAR(fiscal_year) = YEAR(NOW()))")
                            ->get()->result();
        }
 
        //Validation
        private function validation(){
            return [
                ['field' => 'fiscal_year','label' => 'fiscal year','rules' => 'required'],
                ['field' => 'cost','label' => 'cost','rules' => 'required'],
                ['field' => 'start_date','label' => 'start date','rules' => 'required'],
                ['field' => 'end_date','label' => 'end date','rules' => 'required'],
                ['field' => 'training_id','label' => 'training','rules' => 'required'],
                ['field' => 'training_provider_id','label' => 'training provider','rules' => 'required']
            ];
        }

    }
