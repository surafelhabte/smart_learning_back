<?php

   class Enrollment_Model extends CI_Model
   {

        public function __construct()
        {
           parent::__construct();
           $this->load->helper('uuid_gen');
           $this->core_Db=config_item('core_db');

        }

        public function Create($post){
            $this->form_validation->set_rules($this->validation())->set_data($post);
                        
            if($this->form_validation->run()) {
                $post['id'] = uuid_gen();

                $this->db->trans_begin();
                $this->db->insert('tbl_enrollment', $post);                

                if ($this->db->trans_status() === true) {
                    $this->db->trans_commit();
                    return ['status'=>true, 'message' =>'enrollment saved successfully.'];
    
                } else {
                    $this->db->trans_rollback();
                    return ['status'=>false, 'message' =>'unable to save the enrollment.'];
                }

            } else {
                return ['status' => false, 'message' => implode('\n', $this->form_validation->error_array())];

            }
        }

        public function Update($post){        
            $result = $this->db->from('tbl_certification')->where(['enrollment_id'=>$post['id']])->count_all_results();
            
            if($result > 0){
                return ['status'=>false, 'message' =>"you can't update enrollment now. b\c certification already given."];
                
            } else {
                $id =  ['field' => 'id','label' => 'id','rules' => 'required'];

                $this->form_validation->set_rules(array_merge($this->validation(),$id))->set_data($post);
        
                if($this->form_validation->run()) {                
                        
                    $this->db->trans_begin();
                    $this->db->update('tbl_enrollment',$post, ['id' => $post['id']]);
    
                    if($this->db->trans_status() === TRUE){
                        $this->db->trans_commit();
                        return ['status'=>true,'message'=>'enrollment updated successfully.'];
            
                    } else {
                        $this->db->trans_rollback();
                        return ['status'=>false,'message'=>'unable to update enrollment.'];
                    }
    
                } else {
                    return ['status' => false, 'message' => implode('\n', $this->form_validation->error_array())];
    
                }
            }
    
        }
    
        public function Filter($post){
            $select = 'enr.id,emp.employee_id,concat_ws(" ",first_name,middle_name,last_name) as full_name
                      ,sex,position,department_name,tra.title,type';

            $post['fiscal_year'] ?? $post['fiscal_year'] = date("Y");

            return $this->db->select($select)
                            ->from("tbl_enrollment as enr")
                            ->join("$this->core_Db.employee_data as emp","enr.employee_id = emp.employee_id")
                            ->join("$this->core_Db.position as pos","pos.id = emp.position_id")
                            ->join("$this->core_Db.department as dep","dep.id = pos.department_id")
                            ->join("tbl_program as pro","pro.id = enr.program_id")
                            ->join("tbl_training as tra","tra.id = pro.training_id")
                            ->where("YEAR(enr.created_at) = $post[fiscal_year]")
                            ->get()->result(); 
        }

        public function Get($id){ 
            return $this->db->get_where("tbl_enrollment", ["id" => $id])->row();
        }

        public function Delete($id){
            $result = $this->db->from('tbl_certification')->where(['enrollment_id'=>$id])->count_all_results();
                               
            if($result > 0){
                return ['status'=>false, 'message' =>"you can't delete enrollment now. b\c certification already given."];
                
            } else {
                $this->db->trans_begin();
                $this->db->delete('tbl_enrollment', ['id' => $id]);
    
                if ($this->db->trans_status() === true) {
                    $this->db->trans_commit();
                    return ['status'=>true, 'message' =>'enrollment deleted successfully.'];
    
                } else {
                    $this->db->trans_rollback();
                    return ['status'=>false, 'message' =>'unable to delete the enrollment.'];
    
                }
            }

        }

        public function Employees($post){            
            return $this->db->select('emp.employee_id as value,CONCAT(first_name," ",middle_name," => ",department_name) As text')
                            ->from("$this->core_Db.employee_data as emp")
                            ->join("$this->core_Db.position as pos","pos.id = emp.position_id")
                            ->join("$this->core_Db.department as dep","dep.id = pos.department_id")
                            
                            ->order_by("first_name")

                            ->group_start()
                                ->or_like('first_name',$post['keyword'])
                                ->or_like('middle_name',$post['keyword'])
                            ->group_end()
                            ->where("employee_id not in (select employee_id from tbl_enrollment where program_id='$post[program_id]')")
                            
                            ->get()->result();
        }

        public function Programs(){            
            return $this->db->select("pro.id as value,title as text")
                            ->from("tbl_program as pro")
                            ->join("tbl_training as tra","tra.id = pro.training_id")
                            ->where("pro.end_date > current_date()")
                            ->get()->result(); 
        }

        //Validation
        private function validation(){
            return [
                ['field' => 'program_id','label' => 'program','rules' => 'required'],
                ['field' => 'employee_id','label' => 'employee','rules' => 'required']
            ];
        }

    }