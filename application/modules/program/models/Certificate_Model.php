<?php

   class Certificate_Model extends CI_Model
   {

        public function __construct()
        {
           parent::__construct();
           $this->load->helper('uuid_gen');
           $this->core_Db=config_item('core_db');
           $this->load->library('Uploader');

        }

        public function Create($post){
            $this->form_validation->set_rules($this->validation($post))->set_data($post);

            if($this->form_validation->run()) {

                if(!is_null($post['enrollment_id'])){
                    $result = $this->db->from('tbl_certification')->where(['enrollment_id' => $post['enrollment_id']])->count_all_results();

                    if($result > 0){
                        return ['status'=>false, 'message' =>"Certification already given for this enrollment."];
                    } 
                }

                $post['id'] = uuid_gen();
                $post['attchement'] = $this->uploader->Convert_From_Base64($post['attchement'],"certificates/", $post['id']);

                $this->db->trans_begin();
                $this->db->insert('tbl_certification', $post);                

                if ($this->db->trans_status() === true) {
                    $this->db->trans_commit();
                    return ['status'=>true, 'message' =>'certification saved successfully.'];
    
                } else {
                    $this->uploader->delete('uploads/certificates/' . $post['id'] . '.jpeg');
                    $this->db->trans_rollback();
                    return ['status'=>false, 'message' =>'unable to save the certification.'];
                }

            } else {
                return ['status' => false, 'message' => implode('\n', $this->form_validation->error_array())];

            }
        }

        public function Update($post){   

            $result = $this->db->from('tbl_certification as cert')
                               ->join('tbl_enrollment as enr',"enr.id = cert.enrollment_id")  
                               ->join('tbl_certification as pro',"pro.id = enr.program_id")  
                               ->where(['pro.id'=>$post['id']])->count_all_results();
            
            if($result > 0){
                return ['status'=>false, 'message' =>"you can't update program now. b\c certification already given."];
                
            } else {
                $id =  ['field' => 'id','label' => 'id','rules' => 'required'];
                $this->form_validation->set_rules(array_merge($this->validation($post),$id))->set_data($post);
        
                if($this->form_validation->run()) {                
                        
                    $this->db->trans_begin();
                    $post['attchement'] = $this->uploader->Convert_From_Base64($post['attchement'],"certificates/", $post['id']);
                    $this->db->update('tbl_certification',$post, ['id' => $post['id']]);
    
                    if($this->db->trans_status() === TRUE){
                        $this->db->trans_commit();
                        return ['status'=>true,'message'=>'certification updated successfully.'];
            
                    } else {
                        $this->uploader->delete('uploads/certificates/' . $post['id'] . '.jpeg');
                        $this->db->trans_rollback();
                    
                        return ['status'=>false,'message'=>'unable to update certification.'];
                    }
    
                } else {
                    return ['status' => false, 'message' => implode('\n', $this->form_validation->error_array())];
    
                }
            }
    
        }
    
        public function Filter($post){
            $select = 'cert.id,concat_ws(" ",first_name,middle_name,last_name) as full_name,position
                        ,department_name,location,result,IFNULL(training,title) as training';

            $post['fiscal_year'] ?? $post['fiscal_year'] = date("Y");

            return $this->db->select($select)
                            ->from("tbl_certification as cert")
                            ->join("$this->core_Db.employee_data as emp","emp.employee_id = cert.employee_id")
                            ->join("$this->core_Db.position as pos","pos.id = emp.position_id")
                            ->join("$this->core_Db.department as dep","dep.id = pos.department_id")
                            
                            ->join("tbl_enrollment as enr","enr.id = cert.enrollment_id","left")
                            ->join("tbl_program as pro","pro.id = enr.program_id","left")
                            ->join("tbl_training as tra","tra.id = pro.training_id","left")
                            ->order_by("cert.created_at","DESC")
                            ->where(["YEAR(cert.created_at) = '$post[fiscal_year]'"])
                            ->get()->result(); 
        }
     
        public function Get($id){ 
            $select = "id,enrollment_id,result,date,training,attchement,employee_id";

            return $this->db->select($select)->get_where("tbl_certification", ["id" => $id])->row();

        }
      
        public function Delete($id){

            $this->db->trans_begin();
            $this->db->delete('tbl_certification', ['id' => $id]);

            if ($this->db->trans_status() === true) {
                $this->uploader->delete('uploads/certificates/' . $id . '.jpeg');
                $this->db->trans_commit();
                return ['status'=>true, 'message' =>'certificate deleted successfully.'];

            } else {
                $this->db->trans_rollback();
                return ['status'=>false, 'message' =>'unable to delete the certificate.'];

            } 

        }

        public function Enrollments($post){
            return $this->db->select("enr.id,concat(title,' => ',YEAR(pro.created_at)) as title")
                            ->from("tbl_program as pro")
                            ->join("tbl_training as tra","tra.id = pro.training_id")
                            ->join("tbl_enrollment as enr","enr.program_id = pro.id")
                            ->where("end_date < current_date() AND enr.employee_id = '$post[employee_id]'")
                            ->get()->result();
        }

        public function Employees($post){
            return $this->db->select('emp.employee_id,CONCAT(first_name," ",middle_name," => ",department_name) As full_name')
                            ->from("$this->core_Db.employee_data as emp")
                            ->join("$this->core_Db.position as pos","pos.id = emp.position_id")
                            ->join("$this->core_Db.department as dep","dep.id = pos.department_id")
                            ->or_like('first_name',$post['keyword'])
                            ->or_like('middle_name',$post['keyword'])
                            ->get()->result();
        }

        //Validation
        private function validation($post){
            if(is_null($post['enrollment_id'])){
                return [
                    ['field' => 'date','label' => 'Date','rules' => 'required'],
                    ['field' => 'result','label' => 'result','rules' => 'required'],
                    ['field' => 'training','label' => 'training','rules' => 'required'],
                    ['field' => 'employee_id','label' => 'employee_id','rules' => 'required']
                ];
            } else {
                return [
                    ['field' => 'date','label' => 'Date','rules' => 'required'],
                    ['field' => 'result','label' => 'result','rules' => 'required'],
                    ['field' => 'enrollment_id','label' => 'enrollment id','rules' => 'required'],
                    ['field' => 'employee_id','label' => 'employee','rules' => 'required']
                ];
            }
        }

    }
