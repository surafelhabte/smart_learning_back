<?php
use Restserver\Libraries\REST_Controller;
use Restserver\Libraries\REST;

defined('BASEPATH') or exit('No direct script access allowed');
require_once APPPATH . 'libraries/REST_Controller.php';
require_once APPPATH . 'libraries/Format.php';
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, OPTIONS");

class Certificate extends CI_Controller
{
    use REST_Controller {
        REST_Controller::__construct as private __resTraitConstruct;
  }
  
    public function __construct()
    {
        parent::__construct();
        $this->__resTraitConstruct();
        $this->load->library('Validate_Token');
        $this->load->model('Certificate_Model');
    }
 
    public function index_post()
    {
        $response = $this->validate_token->authenticateToken($this->input->request_headers());
        if ($response) {
            $result = $this->Certificate_Model->Create($this->post());
            $this->response($result, REST::HTTP_OK);

        } else {
            $this->response(['Not authorized'], REST::HTTP_OK);

        }
    }
  
    public function Update_post()
    {
        $response = $this->validate_token->authenticateToken($this->input->request_headers());
        if ($response) {
            $result = $this->Certificate_Model->Update($this->post());
            $this->response($result, REST::HTTP_OK);

        } else {
            $this->response(['Not authorized'], REST::HTTP_OK);

        }
    }

    public function Filter_post()
    {
        $response = $this->validate_token->authenticateToken($this->input->request_headers());
        if ($response) {
            $result = $this->Certificate_Model->Filter($this->post());
            $this->response($result, REST::HTTP_OK);

        } else {
            $this->response(['Not authorized'], REST::HTTP_OK);

        }
    }

    public function index_get($id)
    {
        $response = $this->validate_token->authenticateToken($this->input->request_headers());
        if ($response) {
            $result = $this->Certificate_Model->Get($id);
            $this->response($result, REST::HTTP_OK);

        } else {
            $this->response(['Not authorized'], REST::HTTP_OK);

        }
    }

    public function index_delete($id)
    {
        $response = $this->validate_token->authenticateToken($this->input->request_headers());
        if ($response) {
            $result = $this->Certificate_Model->Delete($id);
            $this->response($result, REST::HTTP_OK);

        } else {
            $this->response(['Not authorized'], REST::HTTP_OK);

        }
    }

    public function Enrollments_post()
    {
        $response = $this->validate_token->authenticateToken($this->input->request_headers());
        if ($response) {
            $result = $this->Certificate_Model->Enrollments($this->post());
            $this->response($result, REST::HTTP_OK);

        } else {
            $this->response(['Not authorized'], REST::HTTP_OK);

        }
    }

    public function Employees_post()
    {
        $response = $this->validate_token->authenticateToken($this->input->request_headers());
        if ($response) {
            $result = $this->Certificate_Model->Employees($this->post());
            $this->response($result, REST::HTTP_OK);

        } else {
            $this->response(['Not authorized'], REST::HTTP_OK);

        }
    }
    
}
