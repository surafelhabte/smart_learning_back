<?php

   class Request_Model extends CI_Model
   {
        public function __construct()
        {
           parent::__construct();
           $this->core_Db=config_item('core_db');
        }
 
        public function Filter($post){
            $select = 'req.id,emp.employee_id,concat_ws(" ",first_name,middle_name,last_name) as full_name,
                       position,department_name,location,IFNULL(learning_need,title) as title,
                       IFNULL(req.program_type,tra.type) as program_type,req.created_at';

            $post['fiscal_year'] ?? $post['fiscal_year'] = date("Y");

            return $this->db->select($select)
                            ->from("tbl_request as req")
                            ->join("$this->core_Db.employee_data as emp","req.employee_id = emp.employee_id")
                            ->join("$this->core_Db.position as pos","pos.id = emp.position_id")
                            ->join("$this->core_Db.department as dep","dep.id = pos.department_id")

                            ->join("tbl_program as pro","pro.id = req.program_id","left")
                            ->join("tbl_training as tra","tra.id = pro.training_id","left")

                            ->where(["YEAR(req.created_at) = $post[fiscal_year]"])
                            ->order_by("req.created_at","DESC")
                            ->get()->result();  
        }

        public function Detail($id){

            $select = 'IFNULL(learning_need,title) as training,IFNULL(program_type,type) as type,
                        IFNULL(req.importance,tra.importance) as importance,IFNULL(req.priority,tra.priority) as priority,
                        IFNULL(req.method,tra.method) as method,IFNULL(req.reasons,tra.description) as description,
                        IFNULL(req.trainer,train.name) as trainer,IFNULL(CONCAT(start_date," - ",end_date),program_duration) as program_duration,
                        IFNULL(pro.cost,req.cost) as cost,delivery_time,first_approval, second_approval,req.created_at,
                        concat_ws(" ",first_name,middle_name,last_name) as full_name,position,department_name,location';            

            return $this->db->select($select)
                            ->from("tbl_request as req")
                            ->join("$this->core_Db.employee_data as emp","req.employee_id = emp.employee_id")
                            ->join("$this->core_Db.position as pos","pos.id = emp.position_id")
                            ->join("$this->core_Db.department as dep","dep.id = pos.department_id")

                            ->join("tbl_program as pro","pro.id = req.program_id","left")
                            ->join("tbl_training_provider as train","train.id = pro.training_provider_id","left")
                            ->join("tbl_training as tra","tra.id = pro.training_id","left")

                            ->where("req.id = '$id'")
                            ->get()->row(); 
                              
        }

        public function Approve($post){
            $id = ['field' => 'id','label' => 'id','rules' => 'required'];

            $this->form_validation->set_rules($id)->set_data($post);

            if($this->form_validation->run()) { 

                $this->db->trans_begin();
                $this->db->update('tbl_request',$post, ['id' => $post['id']]);
                    
                if($this->db->trans_status() === TRUE){
                    $this->db->trans_commit();
                    return ['status'=>true,'message'=>'learning request approved successfully.'];
        
                } else {
                    $this->db->trans_rollback();
                    return ['status'=>false,'message'=>'unable to approve learning request.'];
                }

            } else {
                return ['status' => false, 'message' => implode('\n', $this->form_validation->error_array())];

            }
        }

   }    