<?php

   class Training_Model extends CI_Model
   {

        public $date;

        public function __construct()
        {
           parent::__construct();
           $this->load->helper('uuid_gen');

        }

        public function Create($post){
            $this->form_validation->set_rules($this->validation())->set_data($post);
                        
            if($this->form_validation->run()) {
                $post['id'] = uuid_gen();

                $this->db->trans_begin();
                $this->db->insert('tbl_training', $post);                

                if ($this->db->trans_status() === true) {
                    $this->db->trans_commit();
                    return ['status'=>true, 'message' =>'training saved successfully.'];
    
                } else {
                    $this->db->trans_rollback();
                    return ['status'=>false, 'message' =>'unable to save the training.'];
                }

            } else {
                return ['status' => false, 'message' => implode('\n', $this->form_validation->error_array())];

            }
        }

        public function Update($post){                    
            $id =  ['field' => 'id','label' => 'id','rules' => 'required'];

            $this->form_validation->set_rules(array_merge($this->validation(),$id))->set_data($post);
    
            if($this->form_validation->run()) {                   
                $this->db->trans_begin();
                $this->db->update('tbl_training',$post, ['id' => $post['id']]);

                if($this->db->trans_status() === TRUE){
                    $this->db->trans_commit();
                    return ['status'=>true,'message'=>'training updated successfully.'];
        
                } else {
                    $this->db->trans_rollback();
                    return ['status'=>false,'message'=>'unable to update training.'];
                }

            } else {
                return ['status' => false, 'message' => implode('\n', $this->form_validation->error_array())];

            }
    
        }
    
        public function Gets(){
            return $this->db->select("id,title,type,importance,priority,method")
                            ->get("tbl_training")
                            ->result();
        }

        public function Get($id){ 
            $select = "id, title, description, type, importance, priority, method";
            
            return $this->db->select($select)->get_where("tbl_training", ["id" => $id])->row();

        }

        public function Delete($id){
            $result = $this->db->from('tbl_program')->where(['training_id'=>$id])->count_all_results();
                               
            if($result > 0){
                return ['status'=>false, 'message' =>'training already been used in program.'];
                
            } else {
                $this->db->trans_begin();
                $this->db->delete('tbl_training', ['id' => $id]);
                
    
                if ($this->db->trans_status() === true) {
                    $this->db->trans_commit();
                    return ['status'=>true, 'message' =>'training deleted successfully.'];
    
                } else {
                    $this->db->trans_rollback();
                    return ['status'=>false, 'message' =>'unable to delete the training.'];
    
                }
            }

        }

        //Validation
        private function validation(){
            return [
                ['field' => 'title','label' => 'title','rules' => 'required'],
                ['field' => 'description','label' => 'description','rules' => 'required'],
                ['field' => 'type','label' => 'type','rules' => 'required'],
                ['field' => 'importance','label' => 'importance','rules' => 'required'],
                ['field' => 'priority','label' => 'priority','rules' => 'required'],
                ['field' => 'method','label' => 'method','rules' => 'required']
            ];
        }

    }
