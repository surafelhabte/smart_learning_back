<?php

   class Provider_Model extends CI_Model
   {

        public function __construct()
        {
           parent::__construct();
           $this->load->helper('uuid_gen');
           $this->load->library('Uploader');

        }

        public function Create($post){        
            $this->form_validation->set_rules($this->validation())->set_data($post);
                            
            if($this->form_validation->run()) {
                $post['id'] = uuid_gen();
                $post['logo'] = $this->uploader->Convert_From_Base64($post['logo'],"logos/", $post['id']);

                $this->db->trans_begin();
                $this->db->insert('tbl_training_provider', $post);
               
                if ($this->db->trans_status() === true) {
                    $this->db->trans_commit();
                    return ['status'=>true, 'message' =>'provider saved successfully.'];
    
                } else {
                    $this->uploader->delete('uploads/logos/' . $post['id'] . '.jpeg');
                    $this->db->trans_rollback();
                    return ['status'=>false, 'message' =>'unable to save the provider.'];
                }

            } else {
                return ['status' => false, 'message' => implode('\n', $this->form_validation->error_array())];
            }  
        }

        public function Update($post){            
            $id = ['field' => 'id','label' => 'id','rules' => 'required'];

            $this->form_validation->set_rules(array_merge($this->validation(),$id))->set_data($post);
    
            if($this->form_validation->run()) {                                        
                $this->db->trans_begin();
                $post['logo'] = $this->uploader->Convert_From_Base64($post['logo'],"logos/", $post['id']);

                $this->db->update('tbl_training_provider',$post, ['id' => $post['id']]);

                if($this->db->trans_status() === TRUE){
                    $this->db->trans_commit();
                    return ['status'=>true,'message'=>'provider updated successfully.'];
        
                } else {
                    $this->uploader->delete('uploads/logos/' . $post['id'] . '.jpeg');
                    $this->db->trans_rollback();
                    return ['status'=>false,'message'=>'unable to update provider.'];
                }

            } else {
                return ['status' => false, 'message' => implode('\n', $this->form_validation->error_array())];

            }
        }

        public function Gets(){ 
            $select = "id,name,phone,email,website,logo as photo_url";
            
            return $this->db->select($select)->get("tbl_training_provider")->result();

        }

        public function Get($id){            
            return $this->db->get_where("tbl_training_provider",["id" => $id])->row();                       
                                                      
        }

        public function Delete($id){
            $result = $this->db->from("tbl_program")->where(['training_provider_id'=>$id])->count_all_results();
            
            if($result > 0){
                return ['status'=>false, 'message' =>" you can't delete this provider. b\c it gives training. "];
                
            } else {
                $this->db->trans_begin();
                $this->db->delete('tbl_training_provider', ['id' => $id]);
    
                if ($this->db->trans_status() === true) {
                    $this->uploader->delete('uploads/logos/' . $id. '.jpeg');
                    $this->db->trans_commit();
                    return ['status'=>true, 'message' =>'provider deleted successfully.'];
    
                } else {
                    $this->db->trans_rollback();
                    return ['status'=>false, 'message' =>'unable to delete the provider.'];
    
                }
            }
        }

        //Validation
        private function validation(){
            return [
                ['field' => 'name','label' => 'name','rules' => 'required'],
                ['field' => 'phone','label' => 'phone','rules' => 'required'],
                ['field' => 'address','label' => 'address','rules' => 'required']
            ];
        }

   }