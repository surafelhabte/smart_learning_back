<?php

   class Enrollment_Model extends CI_Model
   {
        public function __construct()
        {
           parent::__construct();
        }

        public function Filter($post){
            $select = 'enr.id,tra.title,type,method,enr.created_at';

            return $this->db->select($select)
                            ->from("tbl_enrollment as enr")
                            ->join("tbl_program as pro","pro.id = enr.program_id")
                            ->join("tbl_training as tra","tra.id = pro.training_id")
                            ->order_by("enr.created_at","DESC")
                            ->where(["enr.employee_id = '$post[employee_id]'"])
                            ->get()->result(); 
        } 

    }