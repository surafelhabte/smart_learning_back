<?php

   class Request_Model extends CI_Model
   {
        public $date;

        public function __construct()
        {
           parent::__construct();
           $this->load->helper('uuid_gen');
           $this->core_Db=config_item('core_db');
        }
 
        public function Create($post){
            $this->form_validation->set_rules($this->validation($post))->set_data($post);
                            
            if($this->form_validation->run()) {
                $post['id'] = uuid_gen();

                $this->db->trans_begin();
                $this->db->insert('tbl_request', $post);
               
                if ($this->db->trans_status() === true) {
                    $this->db->trans_commit();
                    return ['status'=>true, 'message' =>'learning request submitted successfully.'];
    
                } else {
                    $this->db->trans_rollback();
                    return ['status'=>false, 'message' =>'unable to submit the request.'];
                }

            } else {
                return ['status' => false, 'message' => implode('\n', $this->form_validation->error_array())];
            } 

        }

        public function Update($post){
            $result = $this->db->from('tbl_request')->where(['id'=>$id,'first_approval'=>null])->count_all_results();
            
            if($result === 0){
                return ['status'=>false, 'message' =>'Learning Request is already approved.'];
                
            } else {
                $id = ['field' => 'id','label' => 'id','rules' => 'required'];
    
                $this->form_validation->set_rules(array_merge($this->validation($post),$id))->set_data($post);
        
                if($this->form_validation->run()) {  

                    $this->db->trans_begin();
                    $this->db->update('tbl_request',$post, ['id' => $post['id']]);
    
                    if($this->db->trans_status() === TRUE){
                        $this->db->trans_commit();
                        return ['status'=>true,'message'=>'learning request updated successfully.'];
            
                    } else {
                        $this->db->trans_rollback();
                        return ['status'=>false,'message'=>'unable to update learning request.'];
                    }
    
                } else {
                    return ['status' => false, 'message' => implode('\n', $this->form_validation->error_array())];
    
                }
            }
        }

        public function Filter($post){
            $select = 'req.id,emp.employee_id,concat_ws(" ",first_name,middle_name,last_name) as full_name,
                       position,department_name,location,IFNULL(learning_need,title) as title,
                       IFNULL(req.program_type,tra.type) as program_type,req.created_at';

            $post['fiscal_year'] ?? $post['fiscal_year'] = date("Y");

            if(array_key_exist('department_id', $post)){
                $where = "pos.department_id = '$post[department_id]' AND YEAR(req.created_at) = '$post[fiscal_year]'";

            } else {
                $where = "(req.employee_id = '$post[employee_id]' OR req.requested_by = '$post[employee_id]')
                           AND YEAR(req.created_at) = '$post[fiscal_year]'";
            }

            return $this->db->select($select)
                            ->from("tbl_request as req")
                            ->join("$this->core_Db.employee_data as emp","req.employee_id = emp.employee_id")
                            ->join("$this->core_Db.position as pos","pos.id = emp.position_id")
                            ->join("$this->core_Db.department as dep","dep.id = pos.department_id")

                            ->join("tbl_program as pro","pro.id = req.program_id","left")
                            ->join("tbl_training as tra","tra.id = pro.training_id","left")

                            ->where($where)

                            ->order_by("req.created_at","DESC")
                            ->get()->result();  
        }

        public function Get($id){ 
            $select = "id,learning_need,program_type,program_duration,reasons,importance,priority,cost,delivery_time,
                        trainer,method,employee_id,program_id";   

            return $this->db->select($select)->get_where('tbl_request',['id'=>$id])->row();
        }

        public function Detail($id){
            
            $select = 'IFNULL(learning_need,title) as training,IFNULL(program_type,type) as type,
                       IFNULL(req.importance,tra.importance) as importance,IFNULL(req.priority,tra.priority) as priority,
                       IFNULL(req.method,tra.method) as method,IFNULL(req.reasons,tra.description) as description,
                       IFNULL(req.trainer,train.name) as trainer,IFNULL(CONCAT(start_date," - ",end_date),program_duration) as program_duration,
                       IFNULL(pro.cost,req.cost) as cost,delivery_time,first_approval, second_approval,req.created_at,
                       concat_ws(" ",first_name,middle_name,last_name) as full_name,position,department_name,location';            

            return $this->db->select($select)
                            ->from("tbl_request as req")
                            ->join("$this->core_Db.employee_data as emp","req.employee_id = emp.employee_id")
                            ->join("$this->core_Db.position as pos","pos.id = emp.position_id")
                            ->join("$this->core_Db.department as dep","dep.id = pos.department_id")

                            ->join("tbl_program as pro","pro.id = req.program_id","left")
                            ->join("tbl_training_provider as train","train.id = pro.training_provider_id","left")
                            ->join("tbl_training as tra","tra.id = pro.training_id","left")

                            ->where("req.id = '$id'")
                            ->get()->row(); 
                              
        }

        public function Approve($post){
            $id = [['field' => 'id','label' => 'id','rules' => 'required']];

            $this->form_validation->set_rules($id)->set_data($post);

            if($this->form_validation->run()) { 

                $this->db->trans_begin();
                $this->db->update('tbl_request',$post, ['id' => $post['id']]);
                    
                if($this->db->trans_status() === TRUE){
                    $this->db->trans_commit();
                    return ['status'=>true,'message'=>'learning request approved successfully.'];
        
                } else {
                    $this->db->trans_rollback();
                    return ['status'=>false,'message'=>'unable to approve learning request.'];
                }

            } else {
                return ['status' => false, 'message' => implode('\n', $this->form_validation->error_array())];

            }
        }

        public function Delete($id){

            $this->db->trans_begin();
            $this->db->delete('tbl_request', ['id' => $id]);

            if ($this->db->trans_status() === true) {

                $this->db->trans_commit();
                return ['status'=>true, 'message' =>'learning request deleted successfully.'];

            } else {
                $this->db->trans_rollback();
                return ['status'=>false, 'message' =>'unable to delete the learning request.'];

            }
            
        }

        public function Employees($post){

            $select = 'emp.employee_id,concat_ws(" ",first_name,middle_name,last_name) as full_name';
 
            return $this->db->select($select)
                            ->from("$this->core_Db.employee_data as emp")
                            ->join("$this->core_Db.position as pos","pos.id = emp.position_id")
                            ->group_start()
                                ->or_like('first_name',$post['keyword'])
                                ->or_like('middle_name',$post['keyword'])
                            ->group_end()

                            ->where(["pos.department_id" => $post['dep_id']])
                            ->get()->result();
        }

        public function Programs($post){
            return $this->db->select("pro.id as value,title as text")
                            ->from("tbl_program as pro")
                            ->join("tbl_training as tra","tra.id = pro.training_id")
                            ->group_start()
                                ->where("program_id not in (select program_id from tbl_request where employee_id = '$post[employee_id]')")
                            ->group_end()
                            ->where("YEAR(fiscal_year) = YEAR(NOW())")
                            ->get()->result(); 
        }
     
        //Validation
        private function validation($post){
            if(is_null($post['program_id'])){
                return [
                    ['field' => 'learning_need','label' => 'learning_need','rules' => 'required'],
                    ['field' => 'program_type','label' => 'program_type','rules' => 'required'],
                    ['field' => 'program_duration','label' => 'program_duration','rules' => 'required'],
                    ['field' => 'reasons','label' => 'reasons','rules' => 'required'],
                    ['field' => 'importance','label' => 'importance','rules' => 'required'],
                    ['field' => 'priority','label' => 'priority','rules' => 'required'],
                    ['field' => 'cost','label' => 'cost','rules' => 'required'],
                    ['field' => 'employee_id','label' => 'employee_id','rules' => 'required']
                ];
            } else {
                return [
                    ['field' => 'program_id','label' => 'program','rules' => 'required'],
                    ['field' => 'employee_id','label' => 'employee','rules' => 'required']
                ];
            }

        }

   }   