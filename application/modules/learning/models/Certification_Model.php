<?php

   class Certification_Model extends CI_Model
   {
        public function __construct()
        {
           parent::__construct();

        }
    
        public function Filter($post){
        
            return $this->db->select('IFNULL(title,training) as training,date,result')
                            ->from("tbl_certification as cert")
                            ->join("tbl_enrollment as enr","enr.id = cert.enrollment_id","left")
                            ->join("tbl_program as pro","pro.id = enr.program_id","left")
                            ->join("tbl_training as tra","tra.id = pro.training_id","left")
                            ->order_by("cert.created_at","DESC")
                            ->where(["cert.employee_id = '$post[employee_id]'"])
                            ->get()->result(); 
        }

    }