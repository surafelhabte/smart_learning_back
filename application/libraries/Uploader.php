<?php defined('BASEPATH') or exit('No direct script access allowed');

class Uploader
{
    public function __construct() {}
 
    public function delete($path)
    {
        if (file_exists($path)) {
            if (unlink($path)) {
                return true;

            } else {
                return false;

            }
        } else {
            return false;

        }
    }

    public function Convert_From_Base64($img, $path, $id)
    {
        if (preg_match('/^data:image\/[A-z]{1,4};base64,/', $img)) {
            $folderPath = "uploads/" . $path;
            $image_parts = explode(";base64,", $img);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type = $image_type_aux[1];
            $image_base64 = base64_decode($image_parts[1]);
            $file = $folderPath . $id . '.jpeg';
            file_put_contents($file, $image_base64);

            return "/" . $file;

        } else {
            return $img;

        } 
    }
    
}
