-- MySQL dump 10.13  Distrib 5.7.26, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: Smart_Learning
-- ------------------------------------------------------
-- Server version	5.7.26-0ubuntu0.19.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_certification`
--

DROP TABLE IF EXISTS `tbl_certification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_certification` (
  `id` varchar(55) NOT NULL,
  `enrollment_id` varchar(55) DEFAULT NULL,
  `result` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `training` varchar(255) DEFAULT NULL,
  `attchement` varchar(255) DEFAULT NULL,
  `employee_id` varchar(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_certification_2_idx` (`employee_id`),
  KEY `fk_tbl_certification_1_idx` (`enrollment_id`),
  CONSTRAINT `fk_tbl_certification_1` FOREIGN KEY (`enrollment_id`) REFERENCES `tbl_enrollment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_certification_2` FOREIGN KEY (`employee_id`) REFERENCES `smart_hrm_ecx`.`employee_data` (`employee_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_certification`
--

LOCK TABLES `tbl_certification` WRITE;
/*!40000 ALTER TABLE `tbl_certification` DISABLE KEYS */;
INSERT INTO `tbl_certification` VALUES ('2fc75a8a-cdbc-11ea-bc13-8a4bd6531d96','bac4ea50-cdbb-11ea-bc13-8a4bd6531d96',54,NULL,NULL,'/uploads/certificates/2fc75a8a-cdbc-11ea-bc13-8a4bd6531d96.jpeg','ECX/1285/2019','2020-07-24 14:44:32'),('732401cf-cf2a-11ea-8af3-f5c603b78fa6','5ab2736f-cd96-11ea-bc13-8a4bd6531d96',98,NULL,NULL,'/uploads/certificates/732401cf-cf2a-11ea-8af3-f5c603b78fa6.jpeg','ECX/0003/2008','2020-07-26 10:26:21'),('a5d50a0b-cf2c-11ea-8af3-f5c603b78fa6',NULL,59,'2020-07-23','Introduction To Php','/uploads/certificates/a5d50a0b-cf2c-11ea-8af3-f5c603b78fa6.jpeg','ECX/0020/2008','2020-07-26 10:42:18'),('f5d4c882-cf22-11ea-b163-20d4c86ecfc1','c16aba8d-cdbb-11ea-bc13-8a4bd6531d96',100,NULL,NULL,'/uploads/certificates/f5d4c882-cf22-11ea-b163-20d4c86ecfc1.jpeg','ECX/0270/2010','2020-07-26 09:33:08');
/*!40000 ALTER TABLE `tbl_certification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_configuration`
--

DROP TABLE IF EXISTS `tbl_configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_configuration` (
  `id` varchar(55) NOT NULL,
  `program_beginning_notification` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_configuration`
--

LOCK TABLES `tbl_configuration` WRITE;
/*!40000 ALTER TABLE `tbl_configuration` DISABLE KEYS */;
INSERT INTO `tbl_configuration` VALUES ('1',NULL);
/*!40000 ALTER TABLE `tbl_configuration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_enrollment`
--

DROP TABLE IF EXISTS `tbl_enrollment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_enrollment` (
  `id` varchar(55) NOT NULL,
  `program_id` varchar(55) DEFAULT NULL,
  `employee_id` varchar(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_enrollment_1_idx` (`program_id`),
  KEY `fk_tbl_enrollment_2_idx` (`employee_id`),
  CONSTRAINT `fk_tbl_enrollment_1` FOREIGN KEY (`program_id`) REFERENCES `tbl_program` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_enrollment_2` FOREIGN KEY (`employee_id`) REFERENCES `smart_hrm_ecx`.`employee_data` (`employee_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_enrollment`
--

LOCK TABLES `tbl_enrollment` WRITE;
/*!40000 ALTER TABLE `tbl_enrollment` DISABLE KEYS */;
INSERT INTO `tbl_enrollment` VALUES ('07fb4836-d002-11ea-ae77-d03745519fc9','25b9eeda-cf4a-11ea-8af3-f5c603b78fa6','ECX/01028/2017','2020-07-27 12:09:33'),('13d9715c-d002-11ea-ae77-d03745519fc9','25b9eeda-cf4a-11ea-8af3-f5c603b78fa6','ECX/1245/2019','2020-07-27 12:09:52'),('18b1f4db-d002-11ea-ae77-d03745519fc9','a3c0cf60-cc0c-11ea-b2c3-85acd112e23c','ECX/0156/2009','2020-07-27 12:10:01'),('1f6e3cd7-d002-11ea-ae77-d03745519fc9','a3c0cf60-cc0c-11ea-b2c3-85acd112e23c','ECX/1207/2019','2020-07-27 12:10:12'),('3a0e5cfa-cfff-11ea-ae77-d03745519fc9','25b9eeda-cf4a-11ea-8af3-f5c603b78fa6','ECX/1376/2019','2020-07-27 11:49:28'),('5ab2736f-cd96-11ea-bc13-8a4bd6531d96','a3c0cf60-cc0c-11ea-b2c3-85acd112e23c','ECX/0003/2008','2020-07-24 10:13:43'),('825be417-d011-11ea-ae77-d03745519fc9','a3c0cf60-cc0c-11ea-b2c3-85acd112e23c','ECX/0519/2012','2020-07-27 14:00:20'),('90cc56b4-d011-11ea-ae77-d03745519fc9','a3c0cf60-cc0c-11ea-b2c3-85acd112e23c','ECX/0557/2013','2020-07-27 14:00:45'),('bac4ea50-cdbb-11ea-bc13-8a4bd6531d96','a3c0cf60-cc0c-11ea-b2c3-85acd112e23c','ECX/1285/2019','2020-07-24 14:41:16'),('c16aba8d-cdbb-11ea-bc13-8a4bd6531d96','a3c0cf60-cc0c-11ea-b2c3-85acd112e23c','ECX/0270/2010','2020-07-24 14:41:27'),('ef26642b-d001-11ea-ae77-d03745519fc9','25b9eeda-cf4a-11ea-8af3-f5c603b78fa6','ECX/0817/2016','2020-07-27 12:08:51'),('f4ed55de-d001-11ea-ae77-d03745519fc9','25b9eeda-cf4a-11ea-8af3-f5c603b78fa6','ECX/1228/2019','2020-07-27 12:09:01'),('fa26e455-d001-11ea-ae77-d03745519fc9','25b9eeda-cf4a-11ea-8af3-f5c603b78fa6','ECX/0024/2008','2020-07-27 12:09:09');
/*!40000 ALTER TABLE `tbl_enrollment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_feedback`
--

DROP TABLE IF EXISTS `tbl_feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_feedback` (
  `id` varchar(55) NOT NULL,
  `by` varchar(20) DEFAULT NULL,
  `for` varchar(20) DEFAULT NULL,
  `comment` text,
  `program_id` varchar(55) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_feedback_1_idx` (`by`),
  KEY `fk_tbl_feedback_2_idx` (`for`),
  KEY `fk_tbl_feedback_3_idx` (`program_id`),
  CONSTRAINT `fk_tbl_feedback_1` FOREIGN KEY (`by`) REFERENCES `smart_hrm_ecx`.`employee_data` (`employee_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_feedback_2` FOREIGN KEY (`for`) REFERENCES `smart_hrm_ecx`.`employee_data` (`employee_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_feedback_3` FOREIGN KEY (`program_id`) REFERENCES `tbl_program` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_feedback`
--

LOCK TABLES `tbl_feedback` WRITE;
/*!40000 ALTER TABLE `tbl_feedback` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_feedback` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_notification`
--

DROP TABLE IF EXISTS `tbl_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `message` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_notification`
--

LOCK TABLES `tbl_notification` WRITE;
/*!40000 ALTER TABLE `tbl_notification` DISABLE KEYS */;
INSERT INTO `tbl_notification` VALUES (1,'program_beginning','.Please be Prepare for this.');
/*!40000 ALTER TABLE `tbl_notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_program`
--

DROP TABLE IF EXISTS `tbl_program`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_program` (
  `id` varchar(55) NOT NULL,
  `fiscal_year` date DEFAULT NULL,
  `cost` int(11) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `training_id` varchar(55) DEFAULT NULL,
  `training_provider_id` varchar(55) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_program_1_idx` (`training_id`),
  KEY `fk_tbl_program_2_idx` (`training_provider_id`),
  CONSTRAINT `fk_tbl_program_1` FOREIGN KEY (`training_id`) REFERENCES `tbl_training` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_program_2` FOREIGN KEY (`training_provider_id`) REFERENCES `tbl_training_provider` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_program`
--

LOCK TABLES `tbl_program` WRITE;
/*!40000 ALTER TABLE `tbl_program` DISABLE KEYS */;
INSERT INTO `tbl_program` VALUES ('1',NULL,NULL,'2020-07-10',NULL,'1',NULL,'2020-07-14 14:46:22'),('25b9eeda-cf4a-11ea-8af3-f5c603b78fa6','2020-07-25',29000,'2020-07-26','2020-07-31','b541f0f4-cf2d-11ea-8af3-f5c603b78fa6','c2425f17-cf20-11ea-b163-20d4c86ecfc1','2020-07-26 14:13:15'),('a3c0cf60-cc0c-11ea-b2c3-85acd112e23c','2020-07-22',500,'2020-07-23','2020-07-21','1','9231','2020-07-22 11:15:24');
/*!40000 ALTER TABLE `tbl_program` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_request`
--

DROP TABLE IF EXISTS `tbl_request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_request` (
  `id` varchar(55) NOT NULL,
  `learning_need` varchar(255) DEFAULT NULL,
  `program_type` varchar(45) DEFAULT NULL,
  `program_duration` varchar(45) DEFAULT NULL,
  `reasons` text,
  `importance` varchar(45) DEFAULT NULL,
  `priority` varchar(45) DEFAULT NULL,
  `cost` varchar(45) DEFAULT NULL,
  `delivery_time` varchar(45) DEFAULT NULL,
  `trainer` text,
  `method` varchar(45) DEFAULT NULL,
  `employee_id` varchar(20) DEFAULT NULL,
  `program_id` varchar(55) DEFAULT NULL,
  `first_approval` tinyint(4) DEFAULT NULL,
  `second_approval` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_request_1_idx` (`employee_id`),
  KEY `fk_tbl_request_2_idx` (`program_id`),
  CONSTRAINT `fk_tbl_request_1` FOREIGN KEY (`employee_id`) REFERENCES `smart_hrm_ecx`.`employee_data` (`employee_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_request_2` FOREIGN KEY (`program_id`) REFERENCES `tbl_program` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_request`
--

LOCK TABLES `tbl_request` WRITE;
/*!40000 ALTER TABLE `tbl_request` DISABLE KEYS */;
INSERT INTO `tbl_request` VALUES ('08df7abb-cd9a-11ea-bc13-8a4bd6531d96','Introduction To C++','Certification','3 Months','It Makes Me a Well Programmer.','Highly Important','High','5000','Q1','Hilco Technology Institute.','on-job training','ECX/01081/2017',NULL,NULL,NULL,'2020-07-24 10:40:04'),('379c45ec-cd99-11ea-bc13-8a4bd6531d96',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ECX/0582/2013','a3c0cf60-cc0c-11ea-b2c3-85acd112e23c',NULL,NULL,'2020-07-24 10:34:13'),('54b5d5d4-cdb2-11ea-bc13-8a4bd6531d96',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'ECX/01107/2018','a3c0cf60-cc0c-11ea-b2c3-85acd112e23c',NULL,NULL,'2020-07-24 13:33:59');
/*!40000 ALTER TABLE `tbl_request` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_sent_notifications`
--

DROP TABLE IF EXISTS `tbl_sent_notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_sent_notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `notification_id` int(11) DEFAULT NULL,
  `employee_id` varchar(55) DEFAULT NULL,
  `program_id` varchar(55) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_sent_notifications_1_idx` (`notification_id`),
  KEY `fk_tbl_sent_notifications_2_idx` (`employee_id`),
  KEY `fk_tbl_sent_notifications_3_idx` (`program_id`),
  CONSTRAINT `fk_tbl_sent_notifications_1` FOREIGN KEY (`notification_id`) REFERENCES `tbl_notification` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_sent_notifications_2` FOREIGN KEY (`employee_id`) REFERENCES `smart_hrm_ecx`.`employee_data` (`employee_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_sent_notifications_3` FOREIGN KEY (`program_id`) REFERENCES `tbl_program` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_sent_notifications`
--

LOCK TABLES `tbl_sent_notifications` WRITE;
/*!40000 ALTER TABLE `tbl_sent_notifications` DISABLE KEYS */;
INSERT INTO `tbl_sent_notifications` VALUES (1,1,'ECX/0001/2008',NULL,'2020-07-14 15:33:31');
/*!40000 ALTER TABLE `tbl_sent_notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_training`
--

DROP TABLE IF EXISTS `tbl_training`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_training` (
  `id` varchar(55) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `type` varchar(45) DEFAULT NULL,
  `importance` varchar(45) DEFAULT NULL,
  `priority` varchar(45) DEFAULT NULL,
  `method` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_training`
--

LOCK TABLES `tbl_training` WRITE;
/*!40000 ALTER TABLE `tbl_training` DISABLE KEYS */;
INSERT INTO `tbl_training` VALUES ('1','Introduction to Java','Introduction to Java','Training','Highly Importance','High','In-house','2020-07-20 09:04:22'),('b541f0f4-cf2d-11ea-8af3-f5c603b78fa6','Introduction To ACNE Accounting','Major Course for Accoutant.','Certification','Highly','Medium','short/long term courses','2020-07-26 10:49:40');
/*!40000 ALTER TABLE `tbl_training` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_training_provider`
--

DROP TABLE IF EXISTS `tbl_training_provider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_training_provider` (
  `id` varchar(55) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `website` varchar(45) DEFAULT NULL,
  `address` text,
  `logo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_training_provider`
--

LOCK TABLES `tbl_training_provider` WRITE;
/*!40000 ALTER TABLE `tbl_training_provider` DISABLE KEYS */;
INSERT INTO `tbl_training_provider` VALUES ('9231','Micro-link Information Technology College','+2510261561567','micro@link.com','www.microlink.edu','Piassa Infront Of Royal College.','/uploads/logos/1.jpeg'),('c2425f17-cf20-11ea-b163-20d4c86ecfc1','Hilco Technology Institute','+2510115508673','sales@hilco.edu','www.hilco.edu','In front Of National Palace.','/uploads/logos/c2425f17-cf20-11ea-b163-20d4c86ecfc1.jpeg');
/*!40000 ALTER TABLE `tbl_training_provider` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'Smart_Learning'
--

--
-- Dumping routines for database 'Smart_Learning'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-27 20:18:46
